const mongoose  = require('mongoose');
// const validator = require('validator');
// const jwt       = require('jsonwebtoken');
const _         = require('lodash');
// const bcrypt    = require('bcryptjs');

const tokenKey = process.env.JWT_SECRET;
var UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 1
    },
    room: {
        type: String,
        required: true,
        minlength: 1
    }
})

UserSchema.methods.toJSON = function () {
    var user = this;
    var userObject = user.toObject();

    return _.pick(userObject, ['_id', 'name']);
}

var User = mongoose.model('User', UserSchema)

module.exports = {User};