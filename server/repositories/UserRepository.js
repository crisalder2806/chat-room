const {User} = require('../models/user');

class Users {
    constructor () {
        this.users = [];
    }

    addUser (name, room) {
        return User.findOne({
            'name': name,
            'room': room
        }).then((user) => {
            if (!user) {
                var newUser = new User({
                    name,
                    room
                });
                return newUser.save().then((doc) => {
                    return doc;
                });
            }

            return user;
        });
    }

    async removeUser (name, room) {
        var user = await User.findOneAndRemove({name, room});
        return (user) ? user : Promise.reject('No user in this room.');
    }

    getUserListInRoom (room) {
        return User.find({room}).then((users) => {
            return (users) ? users : Promise.reject('No user in this room.');
        })
    }

    getUser (id) {
        return User.findOne({_id: id}).then((user) => {
            return (user) ? user : Promise.reject('No user in this room.');
        })
    }
}

module.exports = {Users};